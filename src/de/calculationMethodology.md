# Berechnungsmethodik #

## Leistungsbilanz

Im Zentrum der Simulation steht die Leistungsbilanz. Die Abbildung unten zeigt schematisch die Leistungsflüsse ins und aus dem Schweizer Stromnetz. Die Leistungsbilanz muss jederzeit ausgeglichen sein. Das heisst, die Summe der einfliessenden Leistungen ("p" für production) muss gleich der Summe der ausfliessenden Leistungen ("c" für consumption) sein.

$$\sum \limits_i \boldsymbol{P}_{\textrm{p},i} - \sum \limits_j \boldsymbol{P}_{\textrm{c},j} =0$$

> $\boldsymbol{P}$ kann hier und weiter unten als momentane Leistung $P(t)$ oder als Vektor mit den 35'040 15-Minuten-Durchschnittsleistungen eines Jahres interpretiert werden.

$i$ steht für die verschiedenen Energieproduzenten und $j$ für die Energiekonsumenten, wie in nachfolgender Tabelle dargestellt.

Die Leistungsprofile der nicht regelbaren Stromerzeuger ergeben sich aus den historischen Stromerzeugungs- und Wetterdaten.


|$i$ |$j$ | Beschreibung| Regelbarkeit|
|------------ | -------------| -------------| -------------|
| | dem | Stromverbrauch (ohne Verluste)| Nein |
|therm | | Thermische Kraftwerke| Nein |
|nucl | | Atomkraftwerke| Nein |
|riv | | Flusskraftwerke | Nein |
|wind | | Windkraftwerke | Nein |
|sol | | Photovoltaikanlagen | Nein |
|ud | ud | Benutzerdefinierte Stromproduktion| Nein |
|lake | | Wasserkraftwerke (Stausee) | Ja |
|pump |pump | Pumpspeicherwerke | Ja |
|batt |batt | Strom aus Batteriespeichern | Ja |
|imp |exp | Stromimport | Ja |
| |loss | Verluste | Nein |
*Tabelle: Stromerzeuger und -verbraucher.*

![Hier steht die Bildbeschreibung](../img/energy_balance.svg)
*Abbildung: Schematische Darstellung der Leistungszu- und -abflüsse ins elektrische Netz.*

## Grundlegende Strategie

Es gibt beliebig viele Möglichkeiten, wie eine vorgegebene Stromnachfrage durch die verschiedenen Kraftwerkstypen und Energiespeicher gedeckt werden kann. Darum muss eine Gesetzmässigkeit (Strategie) definiert werden, die eindeutig festlegt, wann welcher Kraftwerks- oder Speichertyp zum Einsatz kommt.

Die verwendete Strategie für das Simulationsprogramm *PowerCheck* basiert auf dem Buch Kraftwerk Schweiz von Anton Gunzinger. Sie umfasst nur zwei Punkte:
1. Es wird möglichst hohe Stromautarkie für die Schweiz angestrebt (es wird möglichst versucht, den momentanen Leistungsbedarf mit inländischen Kraftwerken zu decken).
2. Es werden keine Kraftwerke abgeriegelt (es wird kein Strom "verworfen"). 

Diese Strategie entspricht nicht der heutigen Realität. Für die Kraftwerksbetreiber stehen kommerzielle Interessen im Vordergrund. Die Simulationsergebnisse können deshalb von der Ist-Situation abweichen, insbesondere wird in Realität mehr Strom importiert und exportiert als im Simulationsmodell. Die beschriebene Strategie für das Modell wird gewählt, weil es eindeutig und klar ist, und weil das Modell aufzeigen soll, zu welchem Grad die Schweiz selber ihren Strombedarf decken kann.

## Berechnung der regelbaren Leistungen

Aus der oben beschriebenen Strategie ergeben sich folgende Rechenregeln:
1.	Der Stromendverbrauch sowie Kraftwerke, die nicht regelbar sind, werden zuerst berücksichtigt. Daraus ergibt sich eine Residualleistung $P_\textrm{res}$.

$$P_\textrm{res} = - P_\textrm{c,dem} + P_\textrm{p,term} + P_\textrm{p,nucl} + P_\textrm{p,riv} + P_\textrm{p,wind} + P_\textrm{p,sol} + P_\textrm{p,ud} - P_\textrm{c,ud}$$

2.   a) Falls $P_\textrm{res} > 0$ (Überproduktion) werden die Speicher in der Reihenfolge ihrer Effizienz beladen, nämlich
        i) Beladung der Batteriespeicher, falls möglich
        ii) Beladung der Pumpspeicher, falls möglich
   b) Falls $P_\textrm{res} < 0$ (Unterproduktion) werden die Speicher in der Reihenfolge ihrer Effizienz entladen. Eine Ausnahme bilden die Stauseen, welche mit letzter Priorität entladen werden, weil sie nicht künstlich wieder beladen werden können. Die Reihenfolge der Entladung ist also
       i) Entladung der Batteriespeicher, falls möglich
       ii) Entladung der Pumpspeicher, falls möglich
       iii) Entladung der Stauseen, falls möglich

3. Falls, unter Berücksichtigung der Beladung oder Entladung der Speicher, immernoch ein Leistungsmangel oder -überschuss besteht, wird dieser durch "Import" oder "Export" gedeckt. 

Mit diesen Rechenregeln können, bei gegebenen Anfangsbedingungen am 1. Januar um 00:00 Uhr, sequentiell Zeitintervall für Zeitintervall (jeweils 15 Minuten) alle Leistungsflüsse und die Ladung der Energiespeicher für das ganze Jahr berechnet werden.

![Hier steht die Bildbeschreibung](../img/calculation.svg)
*Abbildung: Berechnungsschema.*

## Berechnung des ohmschen Verlusts

Die  Verlustleistung im Übertragungsnetz wird als konstanter Anteil $f$ der in das Netz eingespeisten Leistung (und somit durch das Netz geleiteten Leistung) angenommen. Das heisst, es gilt

$$P_\textrm{c,loss} = f \cdot \sum \limits_i P_{\textrm{p},i}$$

Es sei $P_\textrm{p,nc}$ die Leistung aller nicht regelbaren Kraftwerke

$$P_\textrm{p,nc} = P_\textrm{p,term} + P_\textrm{p,nucl} + P_\textrm{p,riv} + P_\textrm{p,wind} + P_\textrm{p,sol} + P_\textrm{p,ud}$$

und $P_\textrm{c,nc}$ die Leistung aller nicht regelbaren Verbraucher

$$P_\textrm{c,nc} = P_\textrm{c,end} + P_\textrm{c,ud}$$

und $P_\textrm{stor}$ die Leistung aus den Energiespeichern (positiv: laden, negativ: entladen)

$$P_\textrm{stor} = P_\textrm{p,batt} - P_\textrm{c,batt} + P_\textrm{p,pump} - P_\textrm{c,pump} + P_\textrm{p,lake} + P_\textrm{p,imp} - P_\textrm{c,exp}.$$

Die Leistungsbilanz ergibt
$$P_\textrm{p,nc} + P_\textrm{c,nc} + P_\textrm{stor} - P_\textrm{c,loss} = 0.$$

Für die Berechnung der Verlustleistung $P_\textrm{loss}$ muss unterschieden werden, ob die Speicher geladen oder entladen werden.

**Fall 1**: Speicher werden geladen ($P_\textrm{stor} \le 0$):

$$P_\textrm{c,loss} = f \cdot P_\textrm{p,nc}$$

**Fall 2**: Speicher werden entladen ($P_\textrm{stor} > 0$):

$$P_\textrm{c,loss} = f \cdot (P_\textrm{p,nc} + P_\textrm{stor}) = f \cdot (P_\textrm{c,nc} + P_\textrm{c,loss})$$

und damit

$$P_\textrm{c,loss} = \frac{f}{1-f} \cdot P_\textrm{c,nc}$$

## Berechnung der nicht-regelbaren Leistungen

### Endverbrauch

Es stehen die historischen Verbrauchsprofile $P_\textrm{c,dem,hist}(y)$ in 15-Minuten-Auflösung der vergangenen Jahre $y$ als Datenbasis zur Verfügung. Die im Jahr $y$ verbrauchte Energie $E_\textrm{c,dem,hist}(y)$ berechnet sich daraus wie folgt:

$$E_\textrm{c,dem,hist}(y) = \overline{\boldsymbol{P}}_\textrm{c,dem,hist}(y) \cdot 8760 \; \textrm{h}$$

Diese Energiemenge kann durch den Benutzer / die Benutzerin angepasst werden ($E_\textrm{c,dem}$). Das historische Verbrauchsprofil aus dem ausgewählten Jahr wird entsprechend skaliert:

$$\boldsymbol{P}_\textrm{c,dem} =\frac{E_\textrm{c,dem}}{E_\textrm{c,dem,hist}(y)} \cdot \boldsymbol{P}_\textrm{c,dem,hist}(y)$$

### Atomkraftwerke, thermische Kraftwerke, Flusskraftwerke

Es stehen die historischen Erzeugungsprofile $\boldsymbol{P}_{\textrm{p,}i\textrm{,hist}}(y)$ in 15-Minuten-Auflösung der vergangenen Jahre $y$ als Datenbasis zur Verfügung.

Die installierte Nennleistung ("Spitzenleistung") kann durch den Benutzer / die Benutzerin angepasst werden ($P_{\textrm{p,}i\textrm{,max}}$). Das historische Verbrauchsprofil aus dem ausgewählten Jahr wird entsprechend skaliert:

$$\boldsymbol{P}_\textrm{c,dem} =\frac{P_{\textrm{p,}i\textrm{,max}}}{\textrm{max}(\boldsymbol{P}_{\textrm{p,}i\textrm{,hist}}(y))} \cdot \boldsymbol{P}_{\textrm{p,}i\textrm{,hist}}(y)$$

mit $i = \textrm{nucl, term, riv}$.

### Windkraftwerke

Für die Berechnung der Leistung aus Windkraftwerken werden Winddaten von zwanzig Standorten in der Schweiz verwendet. Aus den gemessenen Windgeschwindigkeiten und unter Berücksichtigung der benutzerdefinierten installierten Nennleistung aller Windkraftwerke wird das Leistungsprofil über den Zeitraum von einem Jahr berechnet. Dieses Leistungsprofil wird mit effektiv gemessen Ertragsdaten verglichen, und das Modell mit einem Faktor kalibiert, so dass Modell und Realität für die vergangenen Jahre übereinstimmen.

| Nr. | Standort | Höhe ü. M. (m) | Messhöhe (m) | Latitude (°N) | Longitude (°E) |
|---------- |------------ | :-----------:| :-----------:| :-----------:| :-----------:|
| 1 | Jussy | 501 | 10 | 46.2314 | 6.2931 |
| 2 | La Dôle | 1670 | 10 | 46.4247 | 6.0994 |
| 3 | Bière | 684 | 10 | 46.5250 | 6.3425 |
| 4 | La Brévine | 1050 | 10 | 46.9839 | 6.6103 |
| 5 | Oron | 828 | 10 | 46.5722 | 6.8583 |
| 6 | La Chaux-de-Fonds | 1017 | 10 | 47.0831 | 6.7922 |
| 7 | Cressier | 430 | 10 | 47.0475 | 7.0592 |
| 8 | Chasseral | 1599 | 10 | 47.1317 | 7.0544 |
| 9 | Bantiger | 942 | 100 | 46.9778 | 7.5286 |
| 10 | Beatenberg | 1560 | 10 | 46.7006 | 7.7722 |
| 11 | Egolzwil | 522 | 10 | 47.1794 | 8.0047 |
| 12 | Hildisrieden | 712 | 10 | 47.1558 | 8.2225 |
| 13 | Schmerikon | 408 | 10 | 47.2250 | 8.9403 |
| 14 | Hörnli | 1144 | 10 | 47.3708 | 8.9417 |
| 15 | Ebnat-Kappel | 623 | 10 | 47.2733 | 9.1086 |
| 16 | Chur | 556 | 10 | 46.8703  | 9.5306 |
| 17 | Arosa | 1878 | 10 | 46.7925  | 9.6789 |
| 18 | Valbella | 1568 | 10 | 46.7550  | 9.5544 |
| 19 | Grimsel Hospiz | 1980 |10  | 46.5717 | 8.3333 |
| 20 | Leytron | 512 | 10 | 46.1856  | 7.2211 |

![Hier steht die Bildbeschreibung](../img/wind_stations.png)
*Abbildung: Standorte der Windmessungen. Blaue Regionen: Geeignete Gebiete für Windkraftanlagen.*

Die Windmessungen erfolgen in der Regel auf einer Höhe von 10 m über Boden (siehe Tabelle). Die Nabenhöhe der Windturbinen wird auf 100 m über Boden angenommen. Die Windgeschwindigkeit $v$ hängt vom Abstand $h$ vom Boden ab:

$$\frac{v_2}{v_1} = \frac{\ln(h_2/z_0)}{\ln(h_1/z_0)}$$

mit der Bodenrauigkeit $z_0$. Bei einer Bodenrauigkeit von 0.1 m ergibt sich für die Umrechnung der gemessenen Windgeschwindigkeit auf 10 m Höhe auf die gewünschte Höhe von 100 m ein Geschwindigkeitsverhältnis von 1.5.

![Hier steht die Bildbeschreibung](../img/wind_rel_power.svg)

### Photovoltaik

### Endverbrauch

## Berechnung des Zufluss der Stauseen