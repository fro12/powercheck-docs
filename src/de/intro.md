# Einführung


> *PowerCheck* ist ein Online-Simulationsprogramm mit 15-minütiger Auflösung für das Schweizerische elektrische Energiesystem mit Fokus auf die Energiewende

Der Schutz des Klimas und die damit verbundene Energiewende stellen eine der grössten Herausforderungen unserer Zeit dar.

Die heutige Energieversorgung in der Schweiz wie auch weltweit basiert hauptsächlich auf fossilen Energieträgern wie Erdöl, Kohle und Erdgas (weltweit: 80 %, Schweiz: 63 % im Jahr 2019). Über kurz oder lang wird Elektrizität an die Stelle des wichtigsten Energieträgers treten.

Die Herausforderung des zukünftigen Elektrizitätsnetzes ist, dass jederzeit gleichviel elektrische Leistung eingespeist wird, wie zur selben Zeit verbraucht wird. Denn trotz der Integration von immer grösser werdenden Mengen Strom aus fluktuierenden erneuerbaren Quellen muss die Stabilität des Stromnetzes sichergestellt sein. In diesem Zusammenhang stellen sich eine Reihe von Fragen:
- Können zusätzliche erneuerbare Stromquellen die Atomkraftwerke ersetzen, wenn diese dereinst abgeschaltet werden?
- Wie kann die Stromversorgung während einer "Dunkelflaute" sichergestellt werden?
- Was hat die zunehmende Elektrifizierung des Strassenverkehrs für Auswirkungen auf das Stromsystem?
- Kann die Schweiz sich selber mit ausreichend Elektrizität versorgen?

Um eine sachliche Diskussion dieser und weiterer Fragestellungen zu ermöglichen, stellen wir das Simulationsprogramm *PowerCheck* der interessierten Öffentlichkeit zur Verfügung. Damit können eigene Szenarien für die zukünftige Stromversorgung der Schweiz in Sekundenschnelle simuliert und ausgewertet werden. Das Simulationsmodell hat eine zeitliche Auflösung von 15 Minuten. Als Basis dienen historische Wetterdaten sowie durch den User zu definierende Kraftwerke und Energiespeicher.

## Idee

Das Simulationsprogramm kann man sich so vorstellen, dass es für jeden Kraftwerks- und Stromspeichertyp jeweils nur eine einzige grosse Anlage gibt, die den ganzen Schweizer Kraftwerks- und Stromspeicherpark repräsentiert. Die Erzeugungsleistung resp. Stromspeicherkapazität dieser wenigen Anlagen kann durch den Benutzer / die Benutzerin angepasst werden.

Genau genommen stimmt diese Betrachtung nicht ganz, denn beispielsweise der Stromertrag einer einzigen Photovoltaikanlage wäre von der Sonneneinstrahlung an einem einzigen Ort abhängig. Für Solar- und Windanlagen werden deshalb die Wetterdaten von zwanzig Standorten in der Schweiz "gemittelt".

## Räumliche und zeitliche Abgrenzung


Die räumliche Abgrenzung des Modells bezieht sich auf die Schweizer Landesgrenzen. Es können Stromflüsse über die Landesgrenzen auftreten, sie werden als *Import* und *Export* deklariert. Die zeitliche Abgrenzung des Modells ist ein Kalenderjahr. Denn ein Jahr ist das kleinste Zeitintervall, in welchem die wesentlichen Aspekte der Saisonalität abgebildet werden können. Speziell beachtet werden müssen die Speicherbeladungen Anfang und Ende Jahr. Sie müssen für ein nachhaltiges Szenario möglichst gleich gross sein.

## Annahmen und Vereinfachungen ##

Folgende Annahmen und Vereinfachungen werden getroffen:
1. Keine geografische Auflösung. Die Standorte der Kraftwerke, Speicher und Verbraucher sowie das Stromnetz werden nicht abgebildet. Das heisst, zwischen zwei beliebigen Punkten innerhalb der Schweiz kann eine unbeschränkt grosse elektrische Leistung übertragen werden.
2. Keine Auflösung der Netzebenen (Hoch-, Mittel-, Niederspannung). Die verschiedenen Netzebenen und Distanzen der Leistungsübertragung werden vernachlässigt. Stattdessen wird jede Leistungsübertragung, egal über welche Distanz oder über welche Netzebenen hinweg, mit einem "pauschalen" Verlustanteil von 6 % beaufschlagt.
3. Energiebilanz ist stets ausgeglichen. Die Energiebilanz muss in jedem Zeitintervall von 15 Minuten ausgeglichen sein. Ein allfälliger Stromüberschuss wird als *Export* deklariert, ein allfälliger Strommangel als *Import*. Umgekehrt können Import und Export als Stromüberschuss resp. Strommangel innerhalb der Schweiz interpretiert werden.
4. Keine Schalttage. Um historische Daten aus unterschiedlichen Jahren miteinander zu kombinieren, werden Schalttage vernachlässigt.
5. Keine Sommerzeit. Die Sommerzeit wird vernachlässigt, alle Zeitpunkte beziehen sich auf Schweizer Winterzeit (UTC+1).

## Basis: Historische Daten

Als Grundlage für die Simulation dienen historische Stromerzeugungs-, Stromverbrauchs- und Wetterdaten. Die zeitliche Auflösung dafür beträgt jeweils 15 Minuten. Zwei Gründe sprechen für die Verwendung von historischen Daten:
1. Es handelt sich jeweils um Messdaten. Diese sind real und entsprechend belastbar.
2. Mit Messdaten aus unterschiedlichen Jahren lassen sich *Sensitivitätsstudien* durchführen. Mit anderen Worten lässt sich durch Änderung des Jahres eines Datensatzes (z.B. Sonneneinstrahlung) prüfen, wie *robust* sich das berechnete Szenario in Bezug auf diese Daten verhält. Denn gerade Sonneneinstrahlungs-, Wind- und Niederschlagsdaten variieren jährlich, aber auch das Stromverbrauchsprofil oder die Stromproduktion von beispielsweise Atomkraftwerken.

Mit *PowerCheck* lassen sich also Aufgaben folgenden Typs lösen:
Wann und wieviel Strom müsste die Schweiz importieren, wenn die Atomkraftwerke vom Netz genommen werden, der Stromverbrauch um 5 % gegenüber 2019 steigt, die installierte Photovoltaikleistung sich gegenüber 2019 verzehnfacht, mit den Niederschlags-, Wind- und Sonnendaten aus dem Jahr 2019?