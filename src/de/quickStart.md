# Erste Schritte

In diesen ersten Schritten erfahren Sie, wie Sie ausgehend vom Ist-Zustand ein neues Szenario für die Energielandschaft Schweiz erstellen können. Sie lernen, Resultate darzustellen und Ihr Szenario mit dem Ist-Zustand zu vergleichen.



## Schritt 1: Ist-Zustand berechnen

Wählen Sie das Szenario "Ist-Zustand" aus und klicken Sie auf "Berechnung starten". Jetzt werden die Stromflüsse eines Jahres berechnet.

![Ist-Zustand auswählen und berechnen](../img/select_ist.gif)



## Schritt 2: Resultate ansehen

Schauen wir uns erst die vier Grafiken an, die nun sichtbar sind. Die erste Grafik zeigt beispielsweise die Stromproduktion in einem Jahr, aufgeteilt nach verschiedenen Energiequellen. Die einzelnen Energiequellen können durch Anklicken in der Legende ein- oder ausgeblendet werden. 

![Arbeiten mit Grafiken](../img/production_graph.gif)

Klicken und Ziehen in den Grafiken ermöglicht das Hineinzoomen. Die zeitliche Auflösung kann über die vier Schaltfelder oben rechts eingestellt werden.

Stromproduktion und -verbrauch sind im Netz immer ausgeglichen. Überschüssige Produktion wird den Speichern zugeführt. Sind diese voll, so wird die Energie exportiert.


## Schritt 3: Szenario verändern

## Schritt 4: Szenarien vergleichen



