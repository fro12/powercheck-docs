# Bedienungsanleitung

## Infobutton-Texte

### EndUser

Hier lässt sich der gesamte Stromverbrauch der Schweiz (Endverbrauch, ohne Netzverluste) einstellen. In der Vergangenheit setzte sich dieser zusammen aus ca. je einem Drittel für Haushalte, Industrie und Dienstleistungen, sowie einem kleinen Teil für Verkehr. Der gesamte Stromverbrauch bewegte sich in den letzten zehn Jahren stabil zwischen 55 und 60 TWh.

#### Elektromobilität

2018 betrug der Energieverbrauch des Strassenverkehrs 82 TWh (Erdölprodukte, unterer Heizwert). Das entspricht einem rein elektrischen Energiebedarf von 25 TWh. (+ 45 % des heutigen Stromverbrauchs)

#### Historische Daten

|          | Jahresbedarf (GWh) |
| :------- | -----------------: |
| **2014** |             61'787 |
| **2015** |             62'626 |
| **2016** |             62'617 |
| **2017** |             62'877 |
| **2018** |             61'984 |
| **2019** |             61'501 |

### Nuclear

Hier lässt sich die Stromerzeugung aus Kernkraft einstellen. In den hinterlegten Jahren 2014 bis 2019 ist die Summe der Stromerzeugung aller Schweizer Kernkraftwerke hinterlegt. Die Stromerzeugung bewegte sich in den letzten zehn Jahren zwischen 20 und 26.5 TWh. In den Jahren 2016 und 2017 war die Energiemenge aufgrund einer Revision des KKWs Beznau (Reaktorblock I) verringert.

|               | Nettoleistung (MW) | Hinweis                     |
| :------------ | -----------------: | :-------------------------- |
| **Beznau**    |                365 | 1. Reaktorblock             |
| **Beznau**    |                365 | 2. Reaktorblock             |
| **Gösgen**    |               1010 |                             |
| **Leibstadt** |               1220 |                             |
| **Mühleberg** |                373 | per 20.12.2019 abgeschaltet |
| **Total**     |               3333 |                             |

#### Bandenergie

Kernkraftwerke sind sog. Bandenergieerzeuger. Das heisst, dass sie stabil und relativ konstant in ihrem Nennleistungsbereich Strom erzeugen können. Ohne diese Bandenergieerzeuger können Stromerzeugungslücken entstehen.

#### Ausstieg

Im Rahmen der Energiestrategie 2050 wurde beschlossen, dass keine neuen Kernkraftwerke mehr gebaut werden dürfen und der komplette Ausstieg aus der Kernenergie bis 2050 vollzogen sein muss.

#### Historische Daten

|          | Installierte Leistung (GW) | Jahresenergieertrag (GWh) |
| :------- | -------------------------: | ------------------------: |
| **2014** |                      3.333 |                    26'370 |
| **2015** |                      2.965 |                    22'095 |
| **2016** |                      2.965 |                    20'235 |
| **2017** |                      2.965 |                    19'499 |
| **2018** |                      3.333 |                    24'414 |
| **2019** |                      3.333 |                    25'280 |

### Thermal

Hier lässt sich die Stromerzeugung aus thermischen Quellen einstellen:

|                            | WKK* | Anzahl Anlagen | Jahres- Energieertrag (GWh) |
| :------------------------- | :--: | -------------: | --------------------------: |
| **Diverse Stromerzeuger**  | nein |             20 |                          11 |
| **KVAs**                   | nein |             27 |                        2026 |
| KVAs                       |  ja  |              3 |                         301 |
| **Gross-WKK in Industrie** |  ja  |             15 |                         382 |
| **Fernheizkraftwerke**     |  ja  |             24 |                         469 |
| **Klein-WKK**              |  ja  |            856 |                         539 |


\* Wärme-Kraftkopplung (WKK): Nachnutzung von Wärme als Energiequelle z.B. in Fernwärmenetzen.

#### Gaskraftwerke in der Schweiz

Zurzeit existieren in der Schweiz keine Gaskraftwerke. Bei neuen Gaskraftwerken schreibt der Bund eine Wärme-Kraftkopplung vor, bei welcher neben der  Stromerzeugung auch die Wärme genutzt wird.

#### Historische Daten

|          | Installierte Leistung (GW) | Jahresenergieertrag (GWh) |
| -------- | -------------------------: | ------------------------: |
| **2014** |                       0.57 |                      3955 |
| **2015** |                       0.56 |                      4376 |
| **2016** |                      0.515 |                      5055 |
| **2017** |                        0.5 |                      5322 |
| **2018** |                      0.495 |                      5716 |
| **2019** |                       0.49 |                      6058 |

### Solar

Hier lässt sich die Stromerzeugung aus Solaranlagen (Photovoltaik) einstellen.

#### «Werte aus»

Durch Anwählen eines Jahres wird das entsprechende Jahresprofil der  Sonneneinstrahlung von 20 Messstationen in der Schweiz ausgewählt. Das  Programm rechnet diese Strahlungsdaten unter Berücksichtigung der  installierten Leistung in elektrische Leistungen um.

#### «Installierte Leistung»

Über den Schieberegler oder das Zahlenfeld kann die installierte Leistung  auch manuell geändert werden. Dabei bleibt die Form des Profils  bestehen.

#### Historische Daten

|          | Installierte Leistung (GW) | Jahresenergieertrag (GWh) |
| :------- | -------------------------: | ------------------------: |
| **2010** |                    0.11090 |                      93.6 |
| **2011** |                    0.22291 |                     168.1 |
| **2012** |                    0.43652 |                     299.5 |
| **2013** |                    0.75556 |                     500.5 |
| **2014** |                    1.06060 |                     841.6 |
| **2015** |                    1.39400 |                    1118.6 |
| **2016** |                    1.66390 |                    1333.4 |
| **2017** |                    1.90580 |                    1682.9 |
| **2018** |                    2.17100 |                    1944.3 |

### Wind

Hier lässt sich die Stromerzeugung aus Windkraftanlagen einstellen.

#### «Werte aus»

Durch Anwählen eines Jahres wird das entsprechende Jahresprofil der  Windgeschwindigkeiten von insgesamt 20 Messstationen ausgewählt. Das  Programm rechnet die Windgeschwindigkeitsdaten unter Berücksichtigung  der installierten Leistung in elektrische Leistungen um.

#### «Installierte Leistung»

Über den Schieberegler oder das Zahlenfeld kann die installierte Leistung  auch manuell geändert werden. Dabei bleibt die Form des Profils  bestehen.

#### Historische Daten

|          | Installierte Leistung (GW) | Jahresenergieertrag (GWh) |
| :------- | -------------------------: | ------------------------: |
| **2010** |                     0.0423 |                      36.6 |
| **2011** |                unverändert |                      70.1 |
| **2012** |                     0.0494 |                      88.1 |
| **2013** |                     0.0603 |                      89.5 |
| **2014** |                unverändert |                     100.9 |
| **2015** |                unverändert |                     110.0 |
| **2016** |                unverändert |                     108.6 |
| **2017** |                unverändert |                     132.6 |
| **2018** |                unverändert |                     121.8 |

### Damm

Hier lässt sich die Stromerzeugung aus Stauseekraftwerken einstellen. Es  gilt zu beachten, dass hier nur Stauseen mit natürlichen Zuflüssen ohne  Pumpfunktion berücksichtigt werden.

#### «Zuflussjahr»

Durch Anwählen eines Jahres wird das entsprechende Jahresprofil der  natürlichen Zuflüsse in die Stauseen eingestellt. Natürliche Zuflüsse  stammen von Niederschlägen und Schneeschmelzen.

#### «Abgabeleistung»

Über den Schieberegler oder das Zahlenfeld kann die Abgabeleistung aller Schweizer Stauseekraftwerke eingestellt werden.

#### Speicherkapazitäten

Hier lassen sich über Schieberegler oder Zahlenfelder die Kapazität (max.  Wasservolumen) der Stauseen einstellen. Einerseits der initiale  Füllstand am 1. Januar sowie die gesamte Kapazität aller Stauseen. Der  initiale Füllstand betrug in den vergangenen zehn Jahren 45 bis 65 % der Gesamtkapazität.

#### Wirkungsgrad

Hier lässt sich der Wirkungsgrad der Energieumwandlung einstellen, welcher  erfahungsgemäss ca. 85 % beträgt. Dieser setzt sich zusammen aus dem  Turbinenwirkungsgrad und dem Generatorwirkungsgrad. Bei  Stauseekraftwerken kommen zumeist Pelton- oder Francisturbinen mit  Wirkungsgraden von ca. 90 % zum Einsatz. Grosse Generatoren zur  Stromerzeugung weisen Wirkungsgrade von ca. 95 % auf.

#### Historische Daten

|          | Installierte Leistung (GW) | Initialer Füllstand (TWh) | Gesammtkapazität (TWh) |
| :------- | -------------------------: | ------------------------: | ---------------------: |
| **2014** |                      7.965 |                     5.245 |                    8.8 |
| **2015** |                      7.966 |                     5.368 |                  8.815 |
| **2016** |                      8.156 |                     4.052 |                  8.805 |
| **2017** |                      8.152 |                     4.466 |                   8.84 |
| **2018** |                      8.223 |                     4.361 |                   8.85 |
| **2019** |                      8.414 |                     5.741 |                   8.85 |

### Pump

Hier lässt sich die Stromerzeugung aus Pumpspeicherkraftwerken einstellen.  Es gilt zu beachten, dass hier nur Stauseen mit künstlichen Zuflüssen  mittels Pumpen berücksichtigt werden.

#### Kapazität

Entscheidend für die Speicherkapazität eines Pumpspeicherkraftwerks ist nicht nur  das Oberbecken, sondern auch das Unterbecken. Nur wenn im Unterbecken  genügend Wasser zum Hochpumpen zur Verfügung steht, kann im Oberbecken  ausreichend Wasser gespeichert werden.

#### Installierte Leistung

Hier lassen sich über Schieberegler oder manuell per Zahleneingabe die  Aufnahmeleistung der Wasserpumpen und die Abgabeleistung der Turbinen  einstellen.

#### Speicherkapazitäten

Hier lassen sich über Schieberegler oder Zahlenfelder die Kapazität (max. Wasservolumen) der Stauseen mit Pumpfunktion, sowie deren initialer Füllstand am 1. Januar einstellen. Falls zum initialen Füllstand keine genaueren Angaben zur Verfügung stehen, kann der Wert auf die Hälfte der Kapazität eingestellt werden. Diese Einstellung hat nur einen kleinen Einfluss auf die gesamte Simulation.

#### Wirkungsgrade

Hier lassen sich die Wirkungsgrade des Turbinen- und des Pumpbetriebes  einstellen. Gängige Werte sind ca. 85 % für den Turbinenbetrieb und 78 % für den Pumpbetrieb. Moderne Pumpspeicherkraftwerke wie z.B.  Linth-Limmern sind mit Francis-Pumpturbinen und Motor-Generatoren  ausgestattet, welche in beide Richtungen nutzbar sind. Im  Turbinenbetrieb arbeiten Francis-Turbinen mit einem Wirkungsgrad von ca. 90 % und im Pumpbetrieb mit einem Wirkungsgrad von ca. 82 %. Der  Wirkungsgrad moderner Motor-Generatoren beträgt in beide  Nutzungsrichtungen ca. 95 %.

### Battery

#### Installierte Leistung

Hier lassen sich über Schieberegler oder Zahlenfelder die maximale Aufnahme- und Abgabeleistung der Batteriespeicher einstellen.

#### EKZ Batteriespeicher

EKZ hat einen Batteriespeicher mit folgenden Spezifikationen in Betrieb:

|                                        |                       |
| :------------------------------------- | :-------------------- |
| **Maximale Ladeleistung**              | 18 MW                 |
| **Maximale Entladeleistung**           | 18 MW                 |
| **Speicherkapazität**                  | 7.5 MWh               |
| **Batterietyp**                        | Lithium-Ionen         |
| **Gesamtanzahl an Batteriemodulen**    | 1428                  |
| **Gewicht pro Container**              | 50 t                  |
| **Anzahl Container**                   | 3                     |
| **Batterielebensdauer**                | 10 Jahre (garantiert) |
| **Hersteller**                         | NEC, LG Chem          |
| **Gesamtkosten**                       | 6'000'000             |

Die Kosten pro Megawattstunde (MWh) Speicherkapazität betragen somit ca. CHF 800'000.

#### Speicherkapazitäten

Hier lassen sich über Schieberegler oder Zahlenfelder die Kapazität der  Batteriespeicher einstellen. Einerseits der initiale Ladestand am 1.  Januar sowie die gesamte Kapazität aller Batteriespeicher. Der initiale  Ladestand kann dabei nicht grösser als die Gesamtkapazität sein.

#### Wirkungsgrade

Hier werden die Wirkungsgrade des Lade- und Entladevorgangs der  Batteriespeicher eingestellt. Erfahrungsgemäss beträgt der Wirkungsgrad  in beide Richtungen ca. 96 %.

### Custom Data

Hier können eigene (benutzerdefinierte) Stromverbrauchs- und/oder  -produktionsprofile hochgeladen werden. Auf diese Weise lassen sich  zusätzliche Stromverbraucher oder -erzeuger in der Berechnung  berücksichtigen. Die Leistungsprofile werden nach erfolgreichem Upload  als Diagramm dargestelt. Die Gesamtenergien der Profile können über  Schieberegler oder Zahlenfelder skaliert werden.

#### Datenformat

Die Leistungsdaten müssen in folgender Form hochgeladen werden:

- 1 Spalte
- 35'040 Zeilen (für jede Viertelstunde einen Leistungswert über die Dauer von 365 Tagen)
- Als CSV-Datei
- Die Leistungsdaten müssen in der Grössenordnung «kW» vorliegen. Die Einheit darf aber nicht Teil des Datensatzes sein.

#### Datenformat

Die Leistungsdaten müssen in folgender Form hochgeladen werden:

#### Beispieldatei

Die Beispieldatei beinhaltet Daten eines zufällig verlaufenden  Leistungsprofils mit einem Mittelwert von ca. 51 kW. Es dient primär zur Veranschaulichung der geforderten Datenformatierung.

