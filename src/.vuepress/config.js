module.exports = {
  dest: 'build/docs/',
  base: '/docs/',
  locales: {
    // The key is the path for the locale to be nested under.
    // As a special case, the default locale can use '/' as its path.
    '/en/': {
      lang: 'en-US', // this will be set as the lang attribute on <html>
      title: 'PowerCheck Documentation',
      description: 'Documentation for the PowerCheck app',
    },
    '/de/': {
      lang: 'de-CH',
      title: 'PowerCheck Dokumentation',
      description: 'Dokumentation zur PowerCheck-App',
    },
  },

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', {
      name: 'theme-color',
      content: 'rgb(222,75,64)',
    }],
    ['meta', {
      name: 'apple-mobile-web-app-capable',
      content: 'yes',
    }],
    ['meta', {
      name: 'apple-mobile-web-app-status-bar-style',
      content: 'black',
    }],
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    // sidebar: [
    //   { title: "Kurzanleitung", children: [""] },
    //   {
    //     title: "Misc",
    //       children: ["more"],
    //   },
    // ],
    locales: {
      '/en/': {
        label: 'English',
        selectText: 'English',
        sidebar: [
          '/en/quickStart',
          '/en/userManual',
          '/en/intro',
          '/en/calculationMethodology',
          '/en/producersConsumers',
          '/en/storage',
          '/en/conclusions',
          '/en/future',
          '/en/sources',
          '/en/syntax',
        ],
      },
      '/de/': {
        label: 'Deutsch',
        selectText: 'Deutsch',
        sidebar: [
          '/de/quickStart',
          '/de/userManual',
          '/de/intro',
          '/de/calculationMethodology',
          '/de/producersConsumers',
          '/de/storage',
          '/de/conclusions',
          '/de/future',
          '/de/sources',
          '/de/syntax',
        ],
      },
    },
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
    [
      'vuepress-plugin-mathjax',
      {
        target: 'chtml',
        macros: {
          '*': '\\times',
        },
      },
    ],
  ],
};
